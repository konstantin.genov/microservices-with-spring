package org.konstantingenov.clients.notification;

import lombok.AllArgsConstructor;

public record NotificationRequest(
        Integer toCustomerId,
        String toCustomerEmail,
        String message
) {

}