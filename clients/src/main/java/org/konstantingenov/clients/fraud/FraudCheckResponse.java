package org.konstantingenov.clients.fraud;

public record FraudCheckResponse(Boolean isFraudster) {
}
