package org.konstantingenov.customer;

import lombok.AllArgsConstructor;
import org.konstantingenov.clients.fraud.FraudCheckResponse;
import org.konstantingenov.clients.fraud.FraudClient;
import org.konstantingenov.clients.notification.NotificationClient;
import org.konstantingenov.clients.notification.NotificationRequest;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;
    private final FraudClient fraudClient;
    private final NotificationClient notificationClient;

    public void registerCustomer(CustomerRegistrationRequest customerRegistrationRequest) {
        Customer customer = Customer.builder()
                .firstName(
                        customerRegistrationRequest.firstName()
                )
                .lastName(
                        customerRegistrationRequest.lastName()
                )
                .email(
                        customerRegistrationRequest.email()
                ).build();
        customerRepository.saveAndFlush(customer);

        FraudCheckResponse fraudCheckResponse = fraudClient.isFraudster(customer.getId());

        if (fraudCheckResponse.isFraudster()) {
            throw new IllegalStateException("This guy is a fraudster!");
        }

        notificationClient.sendNotification(
                new NotificationRequest(
                        customer.getId(),
                        customer.getEmail(),
                        String.format("Greetings %s, welcome to my simple application!",
                                customer.getFirstName())
                )
        );
    }
}
